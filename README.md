# README #


Projekt wizualizacji ruchu kostki D6 do gry poruszającej się wewnątrz zamkniętego pudełka. Pudełko, wewnątrz którego znajduje się płytka Arduino UNO oraz moduł MPU-6050/GY-521 (3-osiowy akcelerometr i żyroskop) pełni funkcję kontrolera.
Odpowiednio machając pudełkiem możemy wykonać rzut wirtualną kostką w aplikacji.


### Rzut z użyciem kursora myszki ###
Początkowa szybkość kątowa, liniowa oraz kierunek toczenia zależą od drogi, jaką przebędzie kursor w czasie gdy lewy przycisk myszy jest wciśnięty.
W przypadku wykonywania rzutu tą metodą nie ma potrzeby podłączania urządzenia do komputera.


![gif wizualizujący aplikację](https://media.giphy.com/media/0AK6w5YOdbXn5dhoaD/giphy.gif)


### Rzut z użyciem ,,magicznego pudełka'' ###
Zamiast kursora myszki można wykorzystać kontroler wyposażony w płytkę arduino UNO oraz moduł MPU6050. 
W tym wypadku konieczna jest konfiguracja połączenia urządzenia z aplikacją.


![gif wizualizujący działanie urządzenia](https://media.giphy.com/media/Z6nP4LLzSuOQaMBAx2/giphy.gif)


### Jak wykonać konfigurację z urządzeniem? ###
Należy kliknąć *Config* a następnie wybrać opcję *Find Board*. W rozwijanym menu w głównym oknie aplikacji pojawią się dostępne urządzenia podłączone do portów komputera. Należy wybrać port z naszym Arduino UNO.
Następnie ponownie w zakładce *Config* wybrać opcję *Connect*

W razie problemu z połączeniem lub połączenia się z niewłaściwym urządzeniem w zakładce *Config* dostępne są także funkcje *Disconnect* oraz *RESTART*.


### Kto jest właścicielem projektu? ###
* Autor: Piotr Gorzelnik