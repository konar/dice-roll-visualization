#include "mainwindow.h"


MainWidget::~MainWidget()
{
    makeCurrent();
    delete texture;
    delete geometries;
    doneCurrent();
}

// reakcja na wciśnięcie przycisku myszy (metoda do testowania, będzię zastąpiona odczytami z akcelerometru i żyroskopu)
void MainWidget::mousePressEvent(QMouseEvent *e)
{
    mousePressPosition = QVector2D(e->localPos());  // zapisz pozycję, w której kursor był wciśnięty
}

// reakcja na zwolnienie przycisku myszy

void MainWidget::mouseReleaseEvent(QMouseEvent *e)
{
    QVector2D diff = QVector2D(e->localPos()) - mousePressPosition;

    // przypisanie X, Y, Z z MPU
    QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized(); // macierz rotacji
    m = n; // zmienna pomocnicza do translacji

    qreal acc = diff.length() / 100.0; // szybkość kątowa kostki jest zależna od odległości, jaką przebył kursor z wciśniętym przyciskiem myszy


    rotationAxis = (rotationAxis * angularSpeed + n*acc).normalized(); // oś obrotu


    angularSpeed += acc;
    linearSpeed = angularSpeed/1000.0;
}



void MainWidget::boxShake(QVector3D accel)
{
    QVector3D n = QVector3D(accel.x(), accel.y(), accel.z()).normalized(); // macierz rotacji
    m = n; // zmienna pomocnicza do translacji
    qreal acc=0;

    acc = ((accLast - accel).length())/40000; // szybkość kątowa kostki zależna od urządzenia

    rotationAxis = (rotationAxis * angularSpeed + n * acc).normalized(); // oś obrotu

    angularSpeed += acc;
    linearSpeed = angularSpeed/400.0;

    accLast = accel;
}


void MainWidget::timerEvent(QTimerEvent *)
{
    angularSpeed *= 0.99; // zmniejszanie się szybkości kostki z czasem



    // zatrzymanie kostki gdy szybkość przekroczy minimalną wartość
    if (angularSpeed < 0.2)
        linearSpeed = 0.0;

    if (angularSpeed < 0.01)
    {
        angularSpeed = 0.0;       
    } else {
        // aktualizacja
        rotation = QQuaternion::fromAxisAndAngle(rotationAxis, angularSpeed) * rotation;

        /* Translacja w osi X */
        if(m.x() > 0)
            translationX += 1.5*linearSpeed;

        if(m.x() < 0)
            translationX -= 1.5*linearSpeed;


        /* Translacja w osi Y */
        if(m.y() > 0)
            translationY -= 0.5*linearSpeed;

        if(m.y() < 0)
            translationY += 0.5*linearSpeed;


        update();
    }
}


void MainWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0.19, 0.19, 0.25, 1); // kolor tła widgetu

    initShaders();
    initTextures();


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    geometries = new GeometryEngine;

    timer.start(12, this);
}

//! [3]
void MainWidget::initShaders()
{
    // Shadery wierzchołków
    if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vshader.glsl"))
        close();

    // Shadery elementów ścianek
    if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fshader.glsl"))
        close();

    if (!program.link())
        close();

    if (!program.bind())
        close();
}


void MainWidget::initTextures()
{  
    texture = new QOpenGLTexture(QImage(":/cube.png").mirrored()); // Załadowanie grafiki cube.png

    // Tryby filtrowania tekstur
    texture->setMinificationFilter(QOpenGLTexture::Nearest);
    texture->setMagnificationFilter(QOpenGLTexture::Linear);

    texture->setWrapMode(QOpenGLTexture::Repeat);
}


void MainWidget::resizeGL(int w, int h)
{
    qreal aspect = qreal(w) / qreal(h ? h : 1);

    const qreal zNear = 3.0, zFar = 7.0, fov = 45.0; // plan bliski, plan daleki, kąt widzenia
    projection.setToIdentity(); // restart
    projection.perspective(fov, aspect, zNear, zFar); // ustawienie perspektywy
}


void MainWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    texture->bind();

    // Obliczenie translacji i rotacji modelu
    QMatrix4x4 matrix;
    matrix.translate(translationX, translationY, -5.0);
    matrix.rotate(rotation);

    program.setUniformValue("mvp_matrix", projection * matrix);
    program.setUniformValue("texture", 0);

    geometries->drawCubeGeometry(&program);
}
