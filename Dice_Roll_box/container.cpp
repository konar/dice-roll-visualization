#include "container.h"


DataStore::DataStore(){}

DataStore::DataStore(char _ID,int _X,int _Y,int _Z)
{
    this->ID = _ID;
    this->X = _X;
    this->Y = _Y;
    this->Z = _Z;
}

void DataStore::setX(int &X_){ X = X_; }

void DataStore::setY(int &Y_){ Y = Y_; }

void DataStore::setZ(int &Z_){ Z = Z_; }

void DataStore::setID(char &ID_){ ID = ID_; }

int DataStore::getX(){ return X; }

int DataStore::getY(){ return Y; }

int DataStore::getZ(){ return Z; }

char DataStore::getID(){ return ID; }
