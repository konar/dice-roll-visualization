var searchData=
[
  ['geometries_20',['geometries',['../class_main_widget.html#aee734e7e5ac07e82fe4d497dac5b0ac1',1,'MainWidget']]],
  ['geometryengine_21',['GeometryEngine',['../class_geometry_engine.html',1,'GeometryEngine'],['../class_geometry_engine.html#a79b2b062df3f9cf3284d745c70ab61cc',1,'GeometryEngine::GeometryEngine()']]],
  ['geometryengine_2ecpp_22',['geometryengine.cpp',['../geometryengine_8cpp.html',1,'']]],
  ['geometryengine_2eh_23',['geometryengine.h',['../geometryengine_8h.html',1,'']]],
  ['getdata_24',['getData',['../class_device.html#a760c00fd63374acf25dc07fe69faf05a',1,'Device']]],
  ['getid_25',['getID',['../class_data_store.html#a3ecc508b0a9f4f37e7efbd7615b0a01a',1,'DataStore']]],
  ['getx_26',['getX',['../class_data_store.html#a021c7c7f244463286cb389c1e79eed01',1,'DataStore']]],
  ['gety_27',['getY',['../class_data_store.html#aff1529599084abf327245638e90056a8',1,'DataStore']]],
  ['getz_28',['getZ',['../class_data_store.html#a4e89cedc0bdad788bceedfd2a1e82ac9',1,'DataStore']]],
  ['gyroscopedata_29',['gyroscopeData',['../class_device.html#addc8a56b7984641247490a9190b46a8b',1,'Device']]],
  ['gyrotocharts_30',['gyroToCharts',['../class_device.html#a54eb8227252dc2c12c34949a9b0b77d5',1,'Device']]]
];
