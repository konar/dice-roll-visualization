#include "mainwindow.h"
#include <QApplication>
#include <QLabel>
#include <QSurfaceFormat>


int main(int argc, char *argv[])
{   
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    //MainWidget widget;

   QFile styleSheetFile("C:/Users/Piotr/Desktop/6sem/WDS/dice-roll-visualization/Dice_Roll_box/Adaptic.qss");
   styleSheetFile.open(QFile::ReadOnly);
   QString styleSheet = QLatin1String(styleSheetFile.readAll());
   a.setStyleSheet(styleSheet);

    MainWindow window;
    window.show();
    return a.exec();
}
