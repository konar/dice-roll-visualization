#ifndef MPU_H
#define MPU_H

#include <QSerialPort>
#include <QObject>
#include <QQueue>
#include <fstream>
#include <QVector3D>
//#include <QQuaternion>
#include <QTime>
//#include <QMessageBox>
#include <stdint.h>
#include <QDataStream>
#include <QDebug>
#include <string.h>
#include <math.h>
#include "container.h"


/*! \class Device
 *   \brief Klasa obsługująca komunikację z urządzeniem
 */
class Device : public QObject
{
    Q_OBJECT

public:
    Device(QObject* parent = nullptr);
    ~Device();


public slots:
    /*!
     * \brief metoda dbierająca dane z urządzenia
     * \param[in] arduinoPort - port do którego podłączone jest urządzenie
     */
    void getData(QSerialPort *arduinoPort);


signals:
    /*!
     * \brief Sygnał wysyłany po odebraniu danych
     */
    void dataReceived();

    /*!
     * \brief Sygnał wysyłany po sprawdzeniu poprawności otrzymanych danych
     */
    void newDeviceValues();

    /*!
     * \brief Sygnał wysyła dane z żyroskopu do wykresów
     * \param[in] QVector3D - wektor surowych danych z żyroskopu
     * \param[in] char - ID czujnika
     */
    void gyroToCharts(const QVector3D&,char);

    /*!
     * \brief Sygnał wysyłana dane z akcelerometru do wykresów
     * \param[in] QVector3D - wektor surowych danych z akcelerometru
     * \param[in] char - ID czujnika
     */
    void accToCharts(const QVector3D&,char);


private:
    DataStore *dataStore; //!< pole do przechowywania odebranych danych z czujników
    std::string data; //!< pole przechowujące  odebranych danych w formie stringa

    QVector3D gyroscopeData; //!< pole przechowujące ostatnie odebrane dane z żyroskopu
    QVector3D accelerometerData; //!< pole przechowujące ostatnie odebrane dane z akcelerometru
};


#endif // MPU_H
