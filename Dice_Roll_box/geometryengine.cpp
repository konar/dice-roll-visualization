#include "geometryengine.h"

#include <QVector2D>
#include <QVector3D>


/*! \struct VertexData
    \brief Struktura do przechowywania pozycji kostki w oknie widgetu
*/
struct VertexData
{
    QVector3D position;
    QVector2D texCoord;
};

//! [0]
GeometryEngine::GeometryEngine()
    : indexBuf(QOpenGLBuffer::IndexBuffer)
{
    initializeOpenGLFunctions();

    // VBO
    arrayBuf.create();
    indexBuf.create();

    initCubeGeometry();
}

GeometryEngine::~GeometryEngine()
{
    arrayBuf.destroy();
    indexBuf.destroy();
}
//! [0]

// rysowanie trójkątów tekstur na każdej ścianie kostki
void GeometryEngine::initCubeGeometry()
{
    VertexData vertices[] = {
        // Ściana 4
        {QVector3D(-0.4f, -0.4f,  0.4f), QVector2D(0.0f, 0.0f)},  // wierzchołek 0
        {QVector3D( 0.4f, -0.4f,  0.4f), QVector2D(0.33f, 0.0f)}, // wierzchołek 1
        {QVector3D(-0.4f,  0.4f,  0.4f), QVector2D(0.0f, 0.5f)},  // wierzchołek 2
        {QVector3D( 0.4f,  0.4f,  0.4f), QVector2D(0.33f, 0.5f)}, // wierzchołek 3

        // Ściana 1
        {QVector3D( 0.4f, -0.4f,  0.4f), QVector2D( 0.0f, 0.5f)}, // wierzchołek 4
        {QVector3D( 0.4f, -0.4f, -0.4f), QVector2D(0.33f, 0.5f)}, // wierzchołek 5
        {QVector3D( 0.4f,  0.4f,  0.4f), QVector2D(0.0f, 1.0f)},  // wierzchołek 6
        {QVector3D( 0.4f,  0.4f, -0.4f), QVector2D(0.33f, 1.0f)}, // wierzchołek 7

        // Ściana 3
        {QVector3D( 0.4f, -0.4f, -0.4f), QVector2D(0.66f, 0.5f)}, // wierzchołek 8
        {QVector3D(-0.4f, -0.4f, -0.4f), QVector2D(1.0f, 0.5f)},  // wierzchołek 9
        {QVector3D( 0.4f,  0.4f, -0.4f), QVector2D(0.66f, 1.0f)}, // wierzchołek 10
        {QVector3D(-0.4f,  0.4f, -0.4f), QVector2D(1.0f, 1.0f)},  // wierzchołek 11

        // Ściana 6
        {QVector3D(-0.4f, -0.4f, -0.4f), QVector2D(0.66f, 0.0f)}, // wierzchołek 12
        {QVector3D(-0.4f, -0.4f,  0.4f), QVector2D(1.0f, 0.0f)},  // wierzchołek 13
        {QVector3D(-0.4f,  0.4f, -0.4f), QVector2D(0.66f, 0.5f)}, // wierzchołek 14
        {QVector3D(-0.4f,  0.4f,  0.4f), QVector2D(1.0f, 0.5f)},  // wierzchołek 15

         // Ściana 5
         {QVector3D(-0.4f, -0.4f, -0.4f), QVector2D(0.33f, 0.0f)}, // wierzchołek 16
         {QVector3D( 0.4f, -0.4f, -0.4f), QVector2D(0.66f, 0.0f)}, // wierzchołek 17
         {QVector3D(-0.4f, -0.4f,  0.4f), QVector2D(0.33f, 0.5f)}, // wierzchołek 18
         {QVector3D( 0.4f, -0.4f,  0.4f), QVector2D(0.66f, 0.5f)}, // wierzchołek 19

         // Ściana 2
         {QVector3D(-0.4f,  0.4f,  0.4f), QVector2D(0.33f, 0.5f)}, // wierzchołek 20
         {QVector3D( 0.4f,  0.4f,  0.4f), QVector2D(0.66f, 0.5f)}, // wierzchołek 21
         {QVector3D(-0.4f,  0.4f, -0.4f), QVector2D(0.33f, 1.0f)}, // wierzchołek 22
         {QVector3D( 0.4f,  0.4f, -0.4f), QVector2D(0.66f, 1.0f)}  // wierzchołek 23
    };


    // paski na kostce
    GLushort indices[] = {
         0,  1,  2,  3,  3,     // Ściana 4 - w0,  w1,  w2,  w3
         4,  4,  5,  6,  7,  7, // Ściana 1 - w4,  w5,  w6,  w7
         8,  8,  9, 10, 11, 11, // Ściana 3 - w8,  w9, w10, w11
        12, 12, 13, 14, 15, 15, // Ściana 6 - w12, w13, w14, w15
        16, 16, 17, 18, 19, 19, // Ściana 5 - w16, w17, w18, w19
        20, 20, 21, 22, 23      // Ściana 2 - w20, w21, w22, w23
    };


    // Transfer danych wierzchołków VBO 0
    arrayBuf.bind();
    arrayBuf.allocate(vertices, 24 * sizeof(VertexData));

    // Transfer indeksóW danych do VBO 1
    indexBuf.bind();
    indexBuf.allocate(indices, 34 * sizeof(GLushort));
}


void GeometryEngine::drawCubeGeometry(QOpenGLShaderProgram *program)
{
    // wybór odpowiedniego VBO przez OpenGL
    arrayBuf.bind();
    indexBuf.bind();

    quintptr offset = 0;  // pozycja

    int vertexLocation = program->attributeLocation("a_position");
    program->enableAttributeArray(vertexLocation);
    program->setAttributeBuffer(vertexLocation, GL_FLOAT, offset, 3, sizeof(VertexData));

    offset += sizeof(QVector3D); // koordynaty tekstur

    int texcoordLocation = program->attributeLocation("a_texcoord");
    program->enableAttributeArray(texcoordLocation);
    program->setAttributeBuffer(texcoordLocation, GL_FLOAT, offset, 2, sizeof(VertexData));

    // Narysowanie geometrii kostki z VBO 1
    glDrawElements(GL_TRIANGLE_STRIP, 34, GL_UNSIGNED_SHORT, nullptr);
}

