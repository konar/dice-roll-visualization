#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <cmath>


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mainWidget = new MainWidget(this);
    mainWidget->resize(831, 375); // rozmiar widgetu kostki (na sztywno, do ulepszenia w przyszłości)
    mainWidget->move(229,240); // pozycja widgetu kostki (na sztywno, do ulepszenia w przyszłości)
    this->layout()->addWidget(mainWidget);

    this->device = new QSerialPort(this);
    this->dev = new Device;
    makePlot();
    Connections();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::Connections()
{
    connect(dev, &Device::gyroToCharts, this, &MainWindow::setChartsValue);
    connect(dev, &Device::accToCharts, this, &MainWindow::setChartsValue);
}



void MainWindow::readFromArduino()
{
  while(this->device->canReadLine())
  {
    this->dev->getData(device);
  }
}


void MainWindow::sendMessageToArduino(QString msg)
{
  if(this->device->isOpen() && this->device->isWritable()) // jeśli port jest otwarty i można wysyłać do niego dane
  {
    qDebug() << "Sending message to Arduino: " + msg;
    this->device->write(msg.toStdString().c_str()); // wysłanie i konwersja danych do tablicy typu char
  } else {
    qDebug() << "Cannot send message. Device not connected!";
  }
}


void MainWindow::on_actionFind_Board_triggered()
{
    ui->comboBoxDevices->clear();

    qDebug() << "Searching for devices...";

    QList<QSerialPortInfo> devices;  // stworzenie listy obiektów devices
    devices = QSerialPortInfo::availablePorts();  // przypisanie wartości


    for(int i=0; i<devices.count(); i++)
    {
      qDebug() << devices.at(i).portName() << devices.at(i).description(); // wypisanie w pętli nazw dostępnych portów i urządzeń
      ui->comboBoxDevices->addItem(devices.at(i).portName() + "\t" + devices.at(i).description()); // wypisanie ich do combo boxa
    }
}


void MainWindow::on_actionConnect_triggered()
{
    if(ui->comboBoxDevices->count() == 0) // jeśli nie znaleziono urządzeń możliwych do połączonia
    {
      qDebug() << "No devices found!";
      return;
    }

    QString comboBoxQString = ui->comboBoxDevices->currentText();
    QStringList portList = comboBoxQString.split("\t");
    QString portName = portList.first();

    this->device->setPortName(portName);

    // Otwarcie portu i jego konfiguracja
    if(!device->isOpen())
    {
        if(device->open(QSerialPort::ReadWrite))
        {
          this->device->setBaudRate(QSerialPort::Baud115200); // prędkość programu wgranego do mikrokontrolera
          this->device->setDataBits(QSerialPort::Data8); // 8 bitów danych
          this->device->setParity(QSerialPort::NoParity); // bez kontroli parzystości
          this->device->setStopBits(QSerialPort::OneStop);
          this->device->setFlowControl(QSerialPort::NoFlowControl); // bez kontroli przepływu

          connect(this->device, SIGNAL(readyRead()), this, SLOT(readFromArduino()));                 

          qDebug() << "Connected to: " << portName << "!";

        } else {
            qDebug() << "Connection failed!";
        }

    } else {
        qDebug() << "Device is already connected!";
        return;
    }    
}


void MainWindow::on_actionDisconnect_triggered()
{

    if(this->device->isOpen())
    {             
      this->device->close();
      qDebug() << "Disconnected.";
    } else {
    qDebug() << "There are not connected devices!";
    }

}


void MainWindow::on_actionRESTART_triggered()
{
    MainWindow::on_actionDisconnect_triggered();
    MainWindow::on_actionConnect_triggered();
}



void MainWindow::makePlot()
{
    // wykresy dla zyroskopu
   QCPGraph *graph1 = ui->plotXAxis->addGraph();
   QCPGraph *graph2 = ui->plotYAxis->addGraph();
   QCPGraph *graph3 = ui->plotZAxis->addGraph();

   // niebieskie
   graph1->setPen(QPen(QColor(40, 110, 255), 3));
   graph2->setPen(QPen(QColor(40, 110, 255), 3));
   graph3->setPen(QPen(QColor(40, 110, 255), 3));

   /**********************************************/

   // wykresy dla akcelerometru
   QCPGraph *graph4 = ui->plotXAxis->addGraph();
   QCPGraph *graph5 = ui->plotYAxis->addGraph();
   QCPGraph *graph6 = ui->plotZAxis->addGraph();

   // zielone
   graph4->setPen(QPen(QColor(0,250,154), 3));
   graph5->setPen(QPen(QColor(0,250,154), 3));
   graph6->setPen(QPen(QColor(0,250,154), 3));

   /**********************************************/

   // rysowanie wykresów
   ui->plotXAxis->graph(0)->rescaleValueAxis(true);
   ui->plotYAxis->graph(0)->rescaleValueAxis(true);
   ui->plotZAxis->graph(0)->rescaleValueAxis(true);

   ui->plotXAxis->graph(1)->rescaleValueAxis(true);
   ui->plotYAxis->graph(1)->rescaleValueAxis(true);
   ui->plotZAxis->graph(1)->rescaleValueAxis(true);


   // białe kolory na wykresach
   ui->plotXAxis->xAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotXAxis->yAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotXAxis->xAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotXAxis->yAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotXAxis->xAxis->setTickLabelColor(Qt::white);
   ui->plotXAxis->yAxis->setTickLabelColor(Qt::white);
   ui->plotXAxis->xAxis->setLabelColor(Qt::white);
   ui->plotXAxis->yAxis->setLabelColor(Qt::white);

   ui->plotYAxis->xAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotYAxis->yAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotYAxis->xAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotYAxis->yAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotYAxis->xAxis->setTickLabelColor(Qt::white);
   ui->plotYAxis->yAxis->setTickLabelColor(Qt::white);
   ui->plotYAxis->xAxis->setLabelColor(Qt::white);
   ui->plotYAxis->yAxis->setLabelColor(Qt::white);

   ui->plotZAxis->xAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotZAxis->yAxis->setBasePen(QPen(Qt::white, 1));
   ui->plotZAxis->xAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotZAxis->yAxis->setTickPen(QPen(Qt::white, 1));
   ui->plotZAxis->xAxis->setTickLabelColor(Qt::white);
   ui->plotZAxis->yAxis->setTickLabelColor(Qt::white);
   ui->plotZAxis->xAxis->setLabelColor(Qt::white);
   ui->plotZAxis->yAxis->setLabelColor(Qt::white);

    // kolor tła wykresów
   QLinearGradient plotGradient;
   plotGradient.setStart(0, 0);
   plotGradient.setFinalStop(0, 350);
   plotGradient.setColorAt(0, QColor(66, 83, 101));
   plotGradient.setColorAt(1, QColor(25, 25, 112));
   ui->plotXAxis->setBackground(plotGradient);
   ui->plotYAxis->setBackground(plotGradient);
   ui->plotZAxis->setBackground(plotGradient);

    // Opisy osi:
    ui->plotXAxis->xAxis->setLabel("Time [s]");
    ui->plotXAxis->yAxis->setLabel("Raw X data");

    ui->plotYAxis->xAxis->setLabel("Time [s]");
    ui->plotYAxis->yAxis->setLabel("Raw Y data");

    ui->plotZAxis->xAxis->setLabel("Time [s]");
    ui->plotZAxis->yAxis->setLabel("Raw Z data");

    /**********************************************/

    // zakres osi Y (oś X jest zależna od czasu)
    ui->plotXAxis->yAxis->setRange(-35000, 35000);
    ui->plotYAxis->yAxis->setRange(-35000, 35000);
    ui->plotZAxis->yAxis->setRange(-35000, 35000);
}



void MainWindow::setChartsValue(const QVector3D &Axis, char ID)
{
   //static int i = 0;

    static QTime prev = QTime::currentTime();
    dt = (float)prev.msecsTo(QTime::currentTime())/1000.0;
    this->mainWidget->boxShake(Axis);

    if(ID=='G')
    {
        Xgyro_y.append(Axis.x());
        Ygyro_y.append(Axis.y());
        Zgyro_y.append(Axis.z());
    }

    if(ID=='A')
    {
        Xacc_y.append(Axis.x());
        Yacc_y.append(Axis.y());
        Zacc_y.append(Axis.z());
    }

    dtTime.append(dt);


    ui->plotXAxis->xAxis->setRange(dt-1, dt+1);
    ui->plotYAxis->xAxis->setRange(dt-1, dt+1);
    ui->plotZAxis->xAxis->setRange(dt-1, dt+1);


    if(ID=='G')
    {
        qDebug() << "ID = G";

        while(dtTime.size()> 200)
            dtTime.erase(dtTime.begin());

        while(Xgyro_y.size()> 200)
            Xgyro_y.erase(Xgyro_y.begin());

        while(Ygyro_y.size()> 200)
            Ygyro_y.erase(Ygyro_y.begin());

        while(Zgyro_y.size()> 200)
            Zgyro_y.erase(Zgyro_y.begin());
    }



    if(ID=='A')
    {
       qDebug() << "ID = A";

        while(dtTime.size()> 200)
                dtTime.erase(dtTime.begin());

        while(Xacc_y.size()> 200)
            Xacc_y.erase(Xacc_y.begin());

        while(Yacc_y.size()> 200)
            Yacc_y.erase(Yacc_y.begin());

        while(Zacc_y.size()> 200)
            Zacc_y.erase(Zacc_y.begin());
    }

   //if(i++<5){ return; } // rysowanie co i-tej danej na wykresach

    // rysowanie wykresów żyroskopu
    ui->plotXAxis->graph(0)->setData(dtTime, Xgyro_y);
    ui->plotYAxis->graph(0)->setData(dtTime, Ygyro_y);
    ui->plotZAxis->graph(0)->setData(dtTime, Zgyro_y);

    // rysowanie wykresów akcelerometru
    ui->plotXAxis->graph(1)->setData(dtTime, Xacc_y);
    ui->plotYAxis->graph(1)->setData(dtTime, Yacc_y);
    ui->plotZAxis->graph(1)->setData(dtTime, Zacc_y);


    ui->plotXAxis->replot();
    ui->plotYAxis->replot();
    ui->plotZAxis->replot();

    ui->plotXAxis->update();
    ui->plotYAxis->update();
    ui->plotZAxis->update();

   // i=0;
}


/* Restart pozycji kostki w głownym widgecie */
void MainWindow::on_pushButtonRestartD6_clicked()
{
   //matrix.translate(0.0, 0.0, -5.0);

    mainWidget = new MainWidget(this);
    mainWidget->resize(831, 375);
    mainWidget->move(229,240);
    this->layout()->addWidget(mainWidget);

}
