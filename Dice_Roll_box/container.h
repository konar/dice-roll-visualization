#ifndef CONTAINER_H
#define CONTAINER_H

#include <QDataStream>
#include <stdint.h>

/*! \class DataStore
    \brief Klasa przechowująca dane odebrane z urządzenia
*/
class DataStore
{
public:
    DataStore();
    DataStore(char _ID, int _X, int _Y, int _Z);

    /*!
     * \brief Metoda ustawiająca ID sensora (A - akcelerometr, G - żyroskop)
     * \param[in] ID_ - identyfikator sensora
    */
    void setID(char &ID_);

    /*!
     * \brief Metoda ustawiająca wartość X
     * \param[in] X_ - wartość na osi X
     */
    void setX(int &X_);
    /*!
     * \brief Metoda ustawiająca wartość Y
     * \param[in] Y_ - wartość na osi Y
     */
    void setY(int &Y_);
    /*!
     * \brief Metoda ustawiająca wartość Z
     * \param[in] Y_ - wartość na osi Z
     */
    void setZ(int &Z_);

    /*!
     * \brief Metoda zwracająca wartość X
     */
    int getX();

    /*!
     * \brief Metoda zwracająca wartość Y
     */
    int getY();

    /*!
     * \brief Metoda zwracająca wartość Z
     */
    int getZ();

    /*!
     * \brief Metoda zwracająca ID
     */
    char getID();

private:
    char ID; //!< identyfikator (A - akceleromter / Z - zyroskop)
    int X, Y, Z; //!< pola przechowywujące przyspieszenia na danych osiach
};

#endif // CONTAINER_H
